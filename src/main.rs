extern crate image;
extern crate clap;
#[macro_use]
extern crate error_chain;

mod errors {
    error_chain!{}
}

use errors::*;

use std::path::Path;
use image::GenericImage;
use clap::{Arg, App};

quick_main!(run);

fn run() -> Result<()> {
    let matches = App::new("Texture convert")
                          .version("0.1")
                          .author("Matus Talcik <matus.talcik@gmail.com>")
                          .about("Converts set of textures into one ORM texture")
                          .arg(Arg::with_name("occlusion")
                               .short("o")
                               .long("occlusion")
                               .value_name("FILE")
                               .takes_value(true))
                          .arg(Arg::with_name("roughness")
                               .short("r")
                               .long("roughness")
                               .takes_value(true))
                          .arg(Arg::with_name("metalness")
                               .short("m")
                               .long("metalness")
                               .takes_value(true))
                          .arg(Arg::with_name("name")
                               .short("n")
                               .long("name")
                               .takes_value(true))
                          .get_matches();

    let occlusion_path = matches.value_of("occlusion");
    let roughness_path = matches.value_of("roughness");
    let metalness_path = matches.value_of("metalness");

    let image_dimensions: (u32, u32);

    if let Some(path) = occlusion_path.or(roughness_path).or(metalness_path) {
        let image = image::open(Path::new(&path)).chain_err(|| "Could not open image")?;
        image_dimensions = image.dimensions();
    } else {
        bail!("You did not supply any arguments");
    }

    let occlusion = if let Some(path) = occlusion_path {
        let image = image::open(Path::new(&path)).chain_err(|| "Could not open image")?;
        match image {
            image::DynamicImage::ImageRgb8(m) => m,
            image::DynamicImage::ImageRgba8(_) => image.to_rgb(),
            _ => {
                bail!("Texture is not in supported format RGB(A)8");
            }
        }
    } else {
        image::ImageBuffer::from_pixel(image_dimensions.0, image_dimensions.1, image::Rgb([255, 255, 255]))
    };

    let roughness = if let Some(path) = roughness_path {
        let image = image::open(Path::new(&path)).chain_err(|| "Could not open image")?;
        match image {
            image::DynamicImage::ImageRgb8(m) => m,
            image::DynamicImage::ImageRgba8(_) => image.to_rgb(),
            _ => {
                bail!("Texture is not in supported format RGB(A)8");
            }
        }
    } else {
        image::ImageBuffer::from_pixel(image_dimensions.0, image_dimensions.1, image::Rgb([0, 0, 0]))
    };

    let metalness = if let Some(path) = metalness_path {
        let image = image::open(Path::new(&path)).chain_err(|| "Could not open image")?;
        match image {
            image::DynamicImage::ImageRgb8(m) => m,
            image::DynamicImage::ImageRgba8(_) => image.to_rgb(),
            _ => {
                bail!("Texture is not in supported format RGB(A)8");
            }
        }
    } else {
        image::ImageBuffer::from_pixel(image_dimensions.0, image_dimensions.1, image::Rgb([0, 0, 0]))
    };

    let mut result: image::RgbImage = image::ImageBuffer::new(image_dimensions.0, image_dimensions.1);

    for (x, y, pixel) in result.enumerate_pixels_mut() {
        *pixel = image::Rgb([occlusion.get_pixel(x, y)[0], roughness.get_pixel(x, y)[0], metalness.get_pixel(x, y)[0]]);
    }

    if let Some(name) = matches.value_of("name") {
        let _ = result.save(&Path::new(name));
    } else {
        let _ = result.save(&Path::new("orm.png"));
    }

    Ok(())
}
